# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta


class CostCategory(metaclass=PoolMeta):
    __name__ = 'cost.manage.category'

    @classmethod
    def _check_child_use_models(cls):
        return super()._check_child_use_models() + ['timesheet.work']
