# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction
from trytond.modules.cost_manage.cost_manage import CategorizedMixin


class Work(CategorizedMixin, metaclass=PoolMeta):
    __name__ = 'timesheet.work'

    @classmethod
    def __register__(cls, module_name):
        pool = Pool()
        WorkCategory = pool.get('timesheet.work.category')
        cursor = Transaction().connection.cursor()

        sql_table = cls.__table__()
        table_h = cls.__table_handler__(module_name)
        work_category = WorkCategory.__table__()

        own_cost_category_exists = table_h.column_exist(
            'own_cost_category')
        cost_concept_category_exists = table_h.column_exist(
            'cost_concept_category')
        costs_category_exists = table_h.column_exist('costs_category')

        super().__register__(module_name)

        if cost_concept_category_exists and costs_category_exists:
            cursor.execute(*sql_table.update(
                columns=[sql_table.cost_category],
                values=[
                    work_category.select(
                        work_category.cost_category,
                        where=(work_category.id
                            == sql_table.cost_concept_category))],
                where=sql_table.costs_category))

            table_h.drop_column('cost_concept_category')
            table_h.drop_column('costs_category')

        if own_cost_category_exists:
            table_h.drop_column('own_cost_category')

    @classmethod
    def copy(cls, records, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default['cost_category'] = None
        return super().copy(records, default=default)
